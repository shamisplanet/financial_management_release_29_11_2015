using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Globalization;
using System.Collections;
using Balancika.Dal;

namespace Balancika.Bll.Base
{
	public class VoucherChequeInfoBase
	{
		protected static Balancika.Dal.VoucherChequeInfoDal dal = new Balancika.Dal.VoucherChequeInfoDal();

		public System.Int64 ChequeId		{ get ; set; }

		public System.String VoucherType		{ get ; set; }

		public System.Int64 VoucherId		{ get ; set; }

		public System.Int32 BankId		{ get ; set; }

		public System.String BankName		{ get ; set; }

		public System.String Branch		{ get ; set; }

		public System.String AccountNo		{ get ; set; }

		public System.String ChequeNo		{ get ; set; }

		public System.String ChequeType		{ get ; set; }

		public System.Decimal ChequeAmount		{ get ; set; }

		public System.Boolean IsPostToLedger		{ get ; set; }

		public System.String SubmiteDate		{ get ; set; }

		public System.Int32 SubmiteToBank		{ get ; set; }


		public  Int32 InsertVoucherChequeInfo()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@ChequeId", ChequeId.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@VoucherType", VoucherType);
			lstItems.Add("@VoucherId", VoucherId.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@BankId", BankId.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@BankName", BankName);
			lstItems.Add("@Branch", Branch);
			lstItems.Add("@AccountNo", AccountNo);
			lstItems.Add("@ChequeNo", ChequeNo);
			lstItems.Add("@ChequeType", ChequeType);
			lstItems.Add("@ChequeAmount", ChequeAmount.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@IsPostToLedger", IsPostToLedger);
			lstItems.Add("@SubmiteDate", SubmiteDate);
			lstItems.Add("@SubmiteToBank", SubmiteToBank.ToString(CultureInfo.InvariantCulture));

			return dal.InsertVoucherChequeInfo(lstItems);
		}

		public  Int32 UpdateVoucherChequeInfo()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@ChequeId", ChequeId.ToString());
			lstItems.Add("@VoucherType", VoucherType);
			lstItems.Add("@VoucherId", VoucherId.ToString());
			lstItems.Add("@BankId", BankId.ToString());
			lstItems.Add("@BankName", BankName);
			lstItems.Add("@Branch", Branch);
			lstItems.Add("@AccountNo", AccountNo);
			lstItems.Add("@ChequeNo", ChequeNo);
			lstItems.Add("@ChequeType", ChequeType);
			lstItems.Add("@ChequeAmount", ChequeAmount.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@IsPostToLedger", IsPostToLedger);
			lstItems.Add("@SubmiteDate", SubmiteDate);
			lstItems.Add("@SubmiteToBank", SubmiteToBank.ToString());

			return dal.UpdateVoucherChequeInfo(lstItems);
		}

		public  Int32 DeleteVoucherChequeInfoByChequeId(Int64 ChequeId)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@ChequeId", ChequeId);

			return dal.DeleteVoucherChequeInfoByChequeId(lstItems);
		}

		public List<VoucherChequeInfo> GetAllVoucherChequeInfo()
		{
			Hashtable lstItems = new Hashtable();
			DataTable dt = dal.GetAllVoucherChequeInfo(lstItems);
			List<VoucherChequeInfo> VoucherChequeInfoList = new List<VoucherChequeInfo>();
			foreach (DataRow dr in dt.Rows)
			{
				VoucherChequeInfoList.Add(GetObject(dr));
			}
			return VoucherChequeInfoList;
		}

		public VoucherChequeInfo  GetVoucherChequeInfoByChequeId(Int64 _ChequeId)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@ChequeId", _ChequeId);

			DataTable dt = dal.GetVoucherChequeInfoByChequeId(lstItems);
			DataRow dr = dt.Rows[0];
			return GetObject(dr);
		}

		protected  VoucherChequeInfo GetObject(DataRow dr)
		{

			VoucherChequeInfo objVoucherChequeInfo = new VoucherChequeInfo();
			objVoucherChequeInfo.ChequeId = (dr["ChequeId"] == DBNull.Value) ? 0 : (Int64)dr["ChequeId"];
			objVoucherChequeInfo.VoucherType = (dr["VoucherType"] == DBNull.Value) ? "" : (String)dr["VoucherType"];
			objVoucherChequeInfo.VoucherId = (dr["VoucherId"] == DBNull.Value) ? 0 : (Int64)dr["VoucherId"];
			objVoucherChequeInfo.BankId = (dr["BankId"] == DBNull.Value) ? 0 : (Int32)dr["BankId"];
			objVoucherChequeInfo.BankName = (dr["BankName"] == DBNull.Value) ? "" : (String)dr["BankName"];
			objVoucherChequeInfo.Branch = (dr["Branch"] == DBNull.Value) ? "" : (String)dr["Branch"];
			objVoucherChequeInfo.AccountNo = (dr["AccountNo"] == DBNull.Value) ? "" : (String)dr["AccountNo"];
			objVoucherChequeInfo.ChequeNo = (dr["ChequeNo"] == DBNull.Value) ? "" : (String)dr["ChequeNo"];
			objVoucherChequeInfo.ChequeType = (dr["ChequeType"] == DBNull.Value) ? "" : (String)dr["ChequeType"];
			objVoucherChequeInfo.ChequeAmount = (dr["ChequeAmount"] == DBNull.Value) ? 0 : (Decimal)dr["ChequeAmount"];
			objVoucherChequeInfo.IsPostToLedger = (dr["IsPostToLedger"] == DBNull.Value) ? false : (Boolean)dr["IsPostToLedger"];
			objVoucherChequeInfo.SubmiteDate = (dr["SubmiteDate"] == DBNull.Value) ? "" : (String)dr["SubmiteDate"];
			objVoucherChequeInfo.SubmiteToBank = (dr["SubmiteToBank"] == DBNull.Value) ? 0 : (Int32)dr["SubmiteToBank"];

			return objVoucherChequeInfo;
		}
	}
}
