using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Globalization;
using System.Collections;
using Balancika.Dal;

namespace Balancika.Bll.Base
{
	public class VoucherDetailBase
	{
		protected static Balancika.Dal.VoucherDetailDal dal = new Balancika.Dal.VoucherDetailDal();

		public System.Int64 VoucherDetailId		{ get ; set; }

		public System.Int64 VoucherId		{ get ; set; }

		public System.Int32 COAId		{ get ; set; }

		public System.String LineDesciption		{ get ; set; }

		public System.Decimal Amount		{ get ; set; }


		public  Int32 InsertVoucherDetail()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@VoucherDetailId", VoucherDetailId.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@VoucherId", VoucherId.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@COAId", COAId.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@LineDesciption", LineDesciption);
			lstItems.Add("@Amount", Amount.ToString(CultureInfo.InvariantCulture));

			return dal.InsertVoucherDetail(lstItems);
		}

		public  Int32 UpdateVoucherDetail()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@VoucherDetailId", VoucherDetailId.ToString());
			lstItems.Add("@VoucherId", VoucherId.ToString());
			lstItems.Add("@COAId", COAId.ToString());
			lstItems.Add("@LineDesciption", LineDesciption);
			lstItems.Add("@Amount", Amount.ToString(CultureInfo.InvariantCulture));

			return dal.UpdateVoucherDetail(lstItems);
		}

		public  Int32 DeleteVoucherDetailByVoucherDetailId(Int64 VoucherDetailId)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@VoucherDetailId", VoucherDetailId);

			return dal.DeleteVoucherDetailByVoucherDetailId(lstItems);
		}

		public List<VoucherDetail> GetAllVoucherDetail()
		{
			Hashtable lstItems = new Hashtable();
			DataTable dt = dal.GetAllVoucherDetail(lstItems);
			List<VoucherDetail> VoucherDetailList = new List<VoucherDetail>();
			foreach (DataRow dr in dt.Rows)
			{
				VoucherDetailList.Add(GetObject(dr));
			}
			return VoucherDetailList;
		}

		public VoucherDetail  GetVoucherDetailByVoucherDetailId(Int64 _VoucherDetailId)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@VoucherDetailId", _VoucherDetailId);

			DataTable dt = dal.GetVoucherDetailByVoucherDetailId(lstItems);
			DataRow dr = dt.Rows[0];
			return GetObject(dr);
		}

		protected  VoucherDetail GetObject(DataRow dr)
		{

			VoucherDetail objVoucherDetail = new VoucherDetail();
			objVoucherDetail.VoucherDetailId = (dr["VoucherDetailId"] == DBNull.Value) ? 0 : (Int64)dr["VoucherDetailId"];
			objVoucherDetail.VoucherId = (dr["VoucherId"] == DBNull.Value) ? 0 : (Int64)dr["VoucherId"];
			objVoucherDetail.COAId = (dr["COAId"] == DBNull.Value) ? 0 : (Int32)dr["COAId"];
			objVoucherDetail.LineDesciption = (dr["LineDesciption"] == DBNull.Value) ? "" : (String)dr["LineDesciption"];
			objVoucherDetail.Amount = (dr["Amount"] == DBNull.Value) ? 0 : (Decimal)dr["Amount"];

			return objVoucherDetail;
		}
	}
}
