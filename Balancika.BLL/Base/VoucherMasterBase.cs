using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Globalization;
using System.Collections;
using Balancika.Dal;

namespace Balancika.Bll.Base
{
	public class VoucherMasterBase
	{
		protected static Balancika.Dal.VoucherMasterDal dal = new Balancika.Dal.VoucherMasterDal();

		public System.Int64 VoucherId		{ get ; set; }

		public System.String VoucherDate		{ get ; set; }

		public System.String VoucherNo		{ get ; set; }

		public System.String VoucherType		{ get ; set; }

		public System.String PaymentMode		{ get ; set; }

		public System.String VoucherDescription		{ get ; set; }

		public System.Int32 UpdateBy		{ get ; set; }

		public System.DateTime UpdateDate		{ get ; set; }

		public System.Int32 ApprovedBy		{ get ; set; }

		public System.DateTime ApprovedDate		{ get ; set; }

		public System.Int32 CompanyId		{ get ; set; }

		public System.DateTime PostDate		{ get ; set; }

		public System.Int32 CostCenterId		{ get ; set; }

		public System.Int32 SourceCOAId		{ get ; set; }

		public System.String VoucherTotal		{ get ; set; }

		public System.String PaymentRef		{ get ; set; }

		public System.Boolean IsPosted		{ get ; set; }

		public System.String Status		{ get ; set; }


		public  Int32 InsertVoucherMaster()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@VoucherId", VoucherId.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@VoucherDate", VoucherDate);
			lstItems.Add("@VoucherNo", VoucherNo);
			lstItems.Add("@VoucherType", VoucherType);
			lstItems.Add("@PaymentMode", PaymentMode);
			lstItems.Add("@VoucherDescription", VoucherDescription);
			lstItems.Add("@UpdateBy", UpdateBy.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@UpdateDate", UpdateDate.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@ApprovedBy", ApprovedBy.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@ApprovedDate", ApprovedDate.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@CompanyId", CompanyId.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@PostDate", PostDate.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@CostCenterId", CostCenterId.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@SourceCOAId", SourceCOAId.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@VoucherTotal", VoucherTotal);
			lstItems.Add("@PaymentRef", PaymentRef);
			lstItems.Add("@IsPosted", IsPosted);
			lstItems.Add("@Status", Status);

			return dal.InsertVoucherMaster(lstItems);
		}

		public  Int32 UpdateVoucherMaster()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@VoucherId", VoucherId.ToString());
			lstItems.Add("@VoucherDate", VoucherDate);
			lstItems.Add("@VoucherNo", VoucherNo);
			lstItems.Add("@VoucherType", VoucherType);
			lstItems.Add("@PaymentMode", PaymentMode);
			lstItems.Add("@VoucherDescription", VoucherDescription);
			lstItems.Add("@UpdateBy", UpdateBy.ToString());
			lstItems.Add("@UpdateDate", UpdateDate.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@ApprovedBy", ApprovedBy.ToString());
			lstItems.Add("@ApprovedDate", ApprovedDate.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@CompanyId", CompanyId.ToString());
			lstItems.Add("@PostDate", PostDate.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@CostCenterId", CostCenterId.ToString());
			lstItems.Add("@SourceCOAId", SourceCOAId.ToString());
			lstItems.Add("@VoucherTotal", VoucherTotal);
			lstItems.Add("@PaymentRef", PaymentRef);
			lstItems.Add("@IsPosted", IsPosted);
			lstItems.Add("@Status", Status);

			return dal.UpdateVoucherMaster(lstItems);
		}

		public  Int32 DeleteVoucherMasterByVoucherId(Int64 VoucherId)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@VoucherId", VoucherId);

			return dal.DeleteVoucherMasterByVoucherId(lstItems);
		}

		public List<VoucherMaster> GetAllVoucherMaster(int CompanyId)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@CompanyId", CompanyId);
			DataTable dt = dal.GetAllVoucherMaster(lstItems);
			List<VoucherMaster> VoucherMasterList = new List<VoucherMaster>();
			foreach (DataRow dr in dt.Rows)
			{
				VoucherMasterList.Add(GetObject(dr));
			}
			return VoucherMasterList;
		}

		public VoucherMaster GetVoucherMasterByVoucherId(int _VoucherId,int _CompanyId)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@VoucherId", _VoucherId);
			lstItems.Add("@CompanyId", _CompanyId);

			DataTable dt = dal.GetVoucherMasterByVoucherId(lstItems);
			DataRow dr = dt.Rows[0];
			return GetObject(dr);
		}

		protected  VoucherMaster GetObject(DataRow dr)
		{

			VoucherMaster objVoucherMaster = new VoucherMaster();
			objVoucherMaster.VoucherId = (dr["VoucherId"] == DBNull.Value) ? 0 : (Int64)dr["VoucherId"];
			objVoucherMaster.VoucherDate = (dr["VoucherDate"] == DBNull.Value) ? "" : (String)dr["VoucherDate"];
			objVoucherMaster.VoucherNo = (dr["VoucherNo"] == DBNull.Value) ? "" : (String)dr["VoucherNo"];
			objVoucherMaster.VoucherType = (dr["VoucherType"] == DBNull.Value) ? "" : (String)dr["VoucherType"];
			objVoucherMaster.PaymentMode = (dr["PaymentMode"] == DBNull.Value) ? "" : (String)dr["PaymentMode"];
			objVoucherMaster.VoucherDescription = (dr["VoucherDescription"] == DBNull.Value) ? "" : (String)dr["VoucherDescription"];
			objVoucherMaster.UpdateBy = (dr["UpdateBy"] == DBNull.Value) ? 0 : (Int32)dr["UpdateBy"];
			objVoucherMaster.UpdateDate = (dr["UpdateDate"] == DBNull.Value) ? DateTime.MinValue : (DateTime)dr["UpdateDate"];
			objVoucherMaster.ApprovedBy = (dr["ApprovedBy"] == DBNull.Value) ? 0 : (Int32)dr["ApprovedBy"];
			objVoucherMaster.ApprovedDate = (dr["ApprovedDate"] == DBNull.Value) ? DateTime.MinValue : (DateTime)dr["ApprovedDate"];
			objVoucherMaster.CompanyId = (dr["CompanyId"] == DBNull.Value) ? 0 : (Int32)dr["CompanyId"];
			objVoucherMaster.PostDate = (dr["PostDate"] == DBNull.Value) ? DateTime.MinValue : (DateTime)dr["PostDate"];
			objVoucherMaster.CostCenterId = (dr["CostCenterId"] == DBNull.Value) ? 0 : (Int32)dr["CostCenterId"];
			objVoucherMaster.SourceCOAId = (dr["SourceCOAId"] == DBNull.Value) ? 0 : (Int32)dr["SourceCOAId"];
			objVoucherMaster.VoucherTotal = (dr["VoucherTotal"] == DBNull.Value) ? "" : (String)dr["VoucherTotal"];
			objVoucherMaster.PaymentRef = (dr["PaymentRef"] == DBNull.Value) ? "" : (String)dr["PaymentRef"];
			objVoucherMaster.IsPosted = (dr["IsPosted"] == DBNull.Value) ? false : (Boolean)dr["IsPosted"];
			objVoucherMaster.Status = (dr["Status"] == DBNull.Value) ? "" : (String)dr["Status"];

			return objVoucherMaster;
		}
	}
}
