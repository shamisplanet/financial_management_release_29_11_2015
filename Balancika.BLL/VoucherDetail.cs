using System;
using System.Text;
using System.Data;
using System.Collections;
using Balancika.Bll.Base;

namespace Balancika.Bll
{
	public class VoucherDetail : Balancika.Bll.Base.VoucherDetailBase
	{
		private static Balancika.Dal.VoucherDetailDal Dal = new Balancika.Dal.VoucherDetailDal();
		public VoucherDetail() : base()
		{
		}
        public int GetMaxVoucherDetailsId()
        {
            return dal.GetMaximumVoucherDetailsID();
        }
	}
}
