using System;
using System.Text;
using System.Data;
using System.Collections;
using Balancika.Bll.Base;

namespace Balancika.Bll
{
	public class VoucherMaster : Balancika.Bll.Base.VoucherMasterBase
	{
		private static Balancika.Dal.VoucherMasterDal Dal = new Balancika.Dal.VoucherMasterDal();
		public VoucherMaster() : base()
		{
		}
        public int GetMaxVoucherMasterId()
        {
            return dal.GetMaximumVoucherMasterID();
        }
	}
}
