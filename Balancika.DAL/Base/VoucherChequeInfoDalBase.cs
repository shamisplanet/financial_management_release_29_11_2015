using System;
using System.Data;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using Balancika.DAL;

namespace Balancika.Dal.Base
{
	public class VoucherChequeInfoDalBase : SqlServerConnection
	{
		public DataTable GetAllVoucherChequeInfo(Hashtable lstData)
		{
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("VoucherChequeInfo", "*", "", lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public DataTable GetVoucherChequeInfoByChequeId(Hashtable lstData)
		{
			string whereCondition = " where VoucherChequeInfo.ChequeId = @ChequeId ";
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("VoucherChequeInfo", "*", whereCondition, lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public int InsertVoucherChequeInfo(Hashtable lstData)
		{
			string sqlQuery ="Insert into VoucherChequeInfo (ChequeId, VoucherType, VoucherId, BankId, BankName, Branch, AccountNo, ChequeNo, ChequeType, ChequeAmount, IsPostToLedger, SubmiteDate, SubmiteToBank) values(@ChequeId, @VoucherType, @VoucherId, @BankId, @BankName, @Branch, @AccountNo, @ChequeNo, @ChequeType, @ChequeAmount, @IsPostToLedger, @SubmiteDate, @SubmiteToBank);";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int UpdateVoucherChequeInfo(Hashtable lstData)
		{
			string sqlQuery = "Update VoucherChequeInfo set VoucherType = @VoucherType, VoucherId = @VoucherId, BankId = @BankId, BankName = @BankName, Branch = @Branch, AccountNo = @AccountNo, ChequeNo = @ChequeNo, ChequeType = @ChequeType, ChequeAmount = @ChequeAmount, IsPostToLedger = @IsPostToLedger, SubmiteDate = @SubmiteDate, SubmiteToBank = @SubmiteToBank where VoucherChequeInfo.ChequeId = @ChequeId;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int DeleteVoucherChequeInfoByChequeId(Hashtable lstData)
		{
			string sqlQuery = "delete from  VoucherChequeInfo where ChequeId = @ChequeId;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
	}
}
