using System;
using System.Data;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using Balancika.DAL;

namespace Balancika.Dal.Base
{
	public class VoucherDetailDalBase : SqlServerConnection
	{
		public DataTable GetAllVoucherDetail(Hashtable lstData)
		{
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("VoucherDetail", "*", "", lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public DataTable GetVoucherDetailByVoucherDetailId(Hashtable lstData)
		{
			string whereCondition = " where VoucherDetail.VoucherDetailId = @VoucherDetailId ";
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("VoucherDetail", "*", whereCondition, lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public int InsertVoucherDetail(Hashtable lstData)
		{
			string sqlQuery ="Insert into VoucherDetail (VoucherDetailId, VoucherId, COAId, LineDesciption, Amount) values(@VoucherDetailId, @VoucherId, @COAId, @LineDesciption, @Amount);";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int UpdateVoucherDetail(Hashtable lstData)
		{
			string sqlQuery = "Update VoucherDetail set VoucherId = @VoucherId, COAId = @COAId, LineDesciption = @LineDesciption, Amount = @Amount where VoucherDetail.VoucherDetailId = @VoucherDetailId;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int DeleteVoucherDetailByVoucherDetailId(Hashtable lstData)
		{
			string sqlQuery = "delete from  VoucherDetail where VoucherDetailId = @VoucherDetailId;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
	}
}
