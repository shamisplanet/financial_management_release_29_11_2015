using System;
using System.Data;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using Balancika.DAL;

namespace Balancika.Dal.Base
{
	public class VoucherMasterDalBase : SqlServerConnection
	{
		public DataTable GetAllVoucherMaster(Hashtable lstData)
		{
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("VoucherMaster", "*", "", lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public DataTable GetVoucherMasterByVoucherId(Hashtable lstData)
		{
			string whereCondition = " where VoucherMaster.VoucherId = @VoucherId ";
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("VoucherMaster", "*", whereCondition, lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public int InsertVoucherMaster(Hashtable lstData)
		{
			string sqlQuery ="Insert into VoucherMaster (VoucherId, VoucherDate, VoucherNo, VoucherType, PaymentMode, VoucherDescription, UpdateBy, UpdateDate, ApprovedBy, ApprovedDate, CompanyId, PostDate, CostCenterId, SourceCOAId, VoucherTotal, PaymentRef, IsPosted, Status) values(@VoucherId, @VoucherDate, @VoucherNo, @VoucherType, @PaymentMode, @VoucherDescription, @UpdateBy, @UpdateDate, @ApprovedBy, @ApprovedDate, @CompanyId, @PostDate, @CostCenterId, @SourceCOAId, @VoucherTotal, @PaymentRef, @IsPosted, @Status);";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int UpdateVoucherMaster(Hashtable lstData)
		{
			string sqlQuery = "Update VoucherMaster set VoucherDate = @VoucherDate, VoucherNo = @VoucherNo, VoucherType = @VoucherType, PaymentMode = @PaymentMode, VoucherDescription = @VoucherDescription, UpdateBy = @UpdateBy, UpdateDate = @UpdateDate, ApprovedBy = @ApprovedBy, ApprovedDate = @ApprovedDate, CompanyId = @CompanyId, PostDate = @PostDate, CostCenterId = @CostCenterId, SourceCOAId = @SourceCOAId, VoucherTotal = @VoucherTotal, PaymentRef = @PaymentRef, IsPosted = @IsPosted, Status = @Status where VoucherMaster.VoucherId = @VoucherId;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int DeleteVoucherMasterByVoucherId(Hashtable lstData)
		{
			string sqlQuery = "delete from  VoucherMaster where VoucherId = @VoucherId;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
	}
}
