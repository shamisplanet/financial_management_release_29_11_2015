using System;
using System.Text;
using System.Data;
using System.Collections;
using BALANCIKA.DAL.Base;

namespace BALANCIKA.DAL
{
	public class JournalMasterDal : BALANCIKA.DAL.Base.JournalMasterDalBase
	{
		public JournalMasterDal() : base()
		{
		}
        public int GetMaxJournalMasterId()
        {
            try
            {
                int maxId = GetMaximumID("JournalMaster", "JournalId", 0, "");
                return maxId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
	}
}
