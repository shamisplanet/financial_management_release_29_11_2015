using System;
using System.Text;
using System.Data;
using System.Collections;
using Balancika.Dal.Base;

namespace Balancika.Dal
{
	public class VoucherDetailDal : Balancika.Dal.Base.VoucherDetailDalBase
	{
		public VoucherDetailDal() : base()
		{
		}
        public int GetMaximumVoucherDetailsID()
        {
            try
            {
                int maxId = GetMaximumID("VoucherDetail", "VoucherDetailId", 0, "");
                return maxId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
	}
}
