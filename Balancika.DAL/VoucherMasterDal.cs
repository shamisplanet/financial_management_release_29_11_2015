using System;
using System.Text;
using System.Data;
using System.Collections;
using Balancika.Dal.Base;

namespace Balancika.Dal
{
	public class VoucherMasterDal : Balancika.Dal.Base.VoucherMasterDalBase
	{
		public VoucherMasterDal() : base()
		{
		}
        public int GetMaximumVoucherMasterID()
        {
            try
            {
                int maxId = GetMaximumID("VoucherMaster", "VoucherId", 0, "");
                return maxId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
	}
}
