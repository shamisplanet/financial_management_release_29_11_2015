﻿using Balancika.BLL;
using BALANCIKA.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Balancika.Rabby
{
    public partial class JournalDetailsRabby : System.Web.UI.Page
    {
        private Company _company = new Company();
        private Users user;
        protected void Page_Load(object sender, EventArgs e)
        {
            journalDetailsInformationDiv.Visible = false;

            user = (Users)Session["user"];
            _company = (Company)Session["Company"];
            if (Session["JournalInformationMessage"] != null)
            {
                string message = (string)Session["JournalInformationMessage"];
                Session["JournalInformationMessage"] = null;
                Alert.Show(message);
            }


            _company = (Company)Session["Company"];
            if (!IsPostBack)
            {
                LoadFirstTime();

            }

            JournalMaster objJournalMaster = new JournalMaster();
            var data = objJournalMaster.GetAllJournalMaster(_company.CompanyId);


            GridView1.DataSource = data;
            GridView1.DataBind();
        }


        public void LoadFirstTime()
        {

            this.LoadJournalDropDownList();
            this.LoadCostCenterDropDownList();
            this.LoadJournalAccountNo();
            Session["JournalMaster"] = null;
            Session["JournalDetailsInformation"] = null;
            journalDetailsInformationDiv.Visible = false;
            journalMasterInformationDiv.Visible = true;

        }

        private void LoadJournalAccountNo()
        {
            BankAccounts anAccount = new BankAccounts();
            List<BankAccounts> accountList = anAccount.GetAllBankAccounts(_company.CompanyId);
            accountNoDropDownList.DataSource = accountList;
            accountNoDropDownList.DataTextField = "AccountTitle";
            accountNoDropDownList.DataValueField = "BankAccountId";
            accountNoDropDownList.DataBind();


        }

        private void LoadJournalDropDownList()
        {
            try
            {
                ListTable aNewListTable = new ListTable();
                List<ListTable> myList = aNewListTable.GetAllListTable(_company.CompanyId);
                journalTypeDropDownList.DataSource = myList;
                journalTypeDropDownList.DataTextField = "ListType";
                journalTypeDropDownList.DataValueField = "ListType";
                journalTypeDropDownList.DataBind();

            }
            catch (Exception exp)
            {
                Alert.Show(exp.Message);
            }
        }
        private void LoadCostCenterDropDownList()
        {
            try
            {
                CostCenter aCost = new CostCenter();
                List<CostCenter> costCenterList = aCost.GetAllCostCenter(_company.CompanyId);
                costCenterDropDownList.DataSource = costCenterList;
                costCenterDropDownList.DataTextField = "CostCenterName";
                costCenterDropDownList.DataValueField = "CostCenterId";
                costCenterDropDownList.DataBind();

            }
            catch (Exception exp)
            {
                Alert.Show(exp.Message);
            }
        }

        protected void btnSaveNewJournalMasterInformation_Click(object sender, EventArgs e)
        {
            JournalMaster newJournalMaster = new JournalMaster();

            newJournalMaster.JournalId = new JournalMaster().GetMaxJournalMasterId() + 1;
            newJournalMaster.JournalDate = (RadDatePicker1.SelectedDate.ToString());
            newJournalMaster.JournalType = journalTypeDropDownList.SelectedText.ToString();
            newJournalMaster.JournalDescription = txtJournalDescription.Value;
            newJournalMaster.UpdateDate = DateTime.Now;
            newJournalMaster.UpdateBy = user.UserId;
            newJournalMaster.ApprovedBy = user.UserId;
            newJournalMaster.ApprovedDate = DateTime.Now;
            newJournalMaster.PostDate = DateTime.Now;
            newJournalMaster.CompanyId = _company.CompanyId;

            newJournalMaster.CostCenterId = int.Parse(costCenterDropDownList.SelectedItem.Value);
            Session["JournalMaster"] = newJournalMaster;


            journalMasterInformationDiv.Visible = false;
            journalDetailsInformationDiv.Visible = true;
        }

        protected void btnSaveJournalSingleDetails_Click(object sender, EventArgs e)
        {
            JournalMaster objJournalMaster = new JournalMaster();
            var data = objJournalMaster.GetAllJournalMaster(_company.CompanyId);
                       

            GridView1.DataSource = data;
            GridView1.DataBind();
        }

        protected void btnClearJournalDetailsInformation_Click(object sender, EventArgs e)
        {

        }

        protected void btnSaveJournalDetailsInformation_Click(object sender, EventArgs e)
        {

        }
    }
}