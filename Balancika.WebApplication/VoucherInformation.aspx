﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="VoucherInformation.aspx.cs" Inherits="Balancika.VoucherInformation" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI, Version=2015.1.225.45, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainHeader" runat="server">
    <h1 id="txtVoucherTypeName" runat="server">Voucher Information </h1>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <section class="form-horizontal">
            <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>
            <div class="box">
                <div id="divVoucherMasterInformation" runat="server" class="box box-primary" style="width: auto">

                    <div class="box-header with-border" style="width: 100%">
                        <h1 style="font-size: 20px; font-weight: bold; color: green">Add New <b>Voucher</b> Information </h1>
                    </div>
                    <div id="divVoucherMasterInformationBody" class="box-body" runat="server">
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label for="txtVoucherNo" class="col-sm-4 control-label">Voucher No.</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" width="60px" name="txtVoucherNo" id="txtVoucherNo" placeholder="Voucher No" runat="server" clientidmode="Static" />
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ">
                                <label for="voucherDatePicker" class="col-sm-4 control-label">Voucher Date</label>
                                <div class="col-xs-8">
                                    <telerik:RadDatePicker Skin="Bootstrap" ID="voucherDatePicker" runat="server" Width="100%" SelectedDate='<%# System.DateTime.Today %>'></telerik:RadDatePicker>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ">
                                <label for="paymentModeDropDownList" class="col-sm-4 control-label">Payment Mode</label>
                                <div class="col-xs-8">
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
                                        <telerik:RadDropDownList ID="paymentModeDropDownList"
                                            Skin="Bootstrap"
                                            runat="server" padding-left="20px"
                                            Width="100%"
                                            AutoPostBack="true"
                                            DefaultMessage="Select Payment Mode">
                                        </telerik:RadDropDownList>

                                    </telerik:RadAjaxPanel>
                                </div>

                            </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="txtVoucherDescription" class="col-sm-4 control-label">Voucher Description</label>
                                    <div class="col-sm-8">
                                        <textarea type="text" class="form-control" width="60px" name="txtVoucherDescription" id="txtVoucherDescription" placeholder="Description" runat="server"></textarea>
                                    </div>
                                </div>
                            </div>
                        <div class="col-sm-6">
                            <div class="form-group ">
                                <label for="costCenterDropDownlist" class="col-sm-4 control-label">Cost Center</label>
                                <div class="col-xs-8">
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server">
                                        <telerik:RadDropDownList ID="costCenterDropDownlist"
                                            Skin="Bootstrap"
                                            runat="server" padding-left="20px"
                                            Width="100%"
                                            AutoPostBack="true"
                                            DefaultMessage="Select Cost Center">
                                        </telerik:RadDropDownList>

                                    </telerik:RadAjaxPanel>
                                </div>

                            </div>
                            </div>
                        <div class="col-sm-6">
                            <div class="form-group ">
                                <label for="coaDropDownList" class="col-sm-4 control-label">Chart of Account</label>
                                <div class="col-xs-8">
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel3" runat="server">
                                        <telerik:RadDropDownList ID="coaDropDownList"
                                            Skin="Bootstrap"
                                            runat="server" padding-left="20px"
                                            Width="100%"
                                            AutoPostBack="true"
                                            DefaultMessage="Select Chart of Account">
                                        </telerik:RadDropDownList>

                                    </telerik:RadAjaxPanel>
                                </div>

                            </div>
                            </div>
                         <div class="col-md-6">
                            <div class="form-group ">
                                <label for="txtVoucherTotal" class="col-sm-4 control-label">Voucher Total</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" width="60px" name="txtVoucherTotal" id="txtVoucherTotal" placeholder="Voucher Total" runat="server" clientidmode="Static" />
                                </div>

                            </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group ">
                                <label for="txtPaymentRef" class="col-sm-4 control-label">Payment Reference</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" width="60px" name="txtPaymentRef" id="txtPaymentRef" placeholder="Payment Reference" runat="server" clientidmode="Static" />
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <asp:Button ID="btnSaveVoucherMasterInfomation" runat="server" ClientIDMode="Static" CssClass="btn btn-success" Text="Save Voucher Master Information" OnClick="btnSaveVoucherMasterInfomation_Click" />
                              
                                 <input class="btn btn-warning" runat="server" onserverclick="btnVoucherMasterInfomationClear_Click" type="button" value="Clear Master Information" />
                            </div>
                        </div>
                        

                        </div>
                    </div>
                
                <div id="divVoucherDetailsInformation" runat="server" class="box box-primary" style="width: auto">
                     <div class="box-header with-border" style="width: 100%">
                        <h1 style="font-size: 20px; font-weight: bold; color: green">Add  <b>Voucher</b> Details Information </h1>
                    </div>
                    <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="txtVoucherDetailsDescription" class="col-sm-4 control-label">Description</label>
                                    <div class="col-sm-8">
                                        <textarea type="text" class="form-control" width="60px" name="txtVoucherDetailsDescription" id="txtVoucherDetailsDescription" placeholder="Description" runat="server"></textarea>
                                    </div>
                                </div>
                            </div>
                    
                     <div class="col-md-6">
                            <div class="form-group ">
                                <label for="txtVoucherAmount" class="col-sm-4 control-label">Amount</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" width="60px" name="txtVoucherAmount" id="txtVoucherAmount" placeholder="Amount" runat="server" clientidmode="Static" />
                                </div>

                            </div>
                        </div>
                     <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <asp:Button ID="btnSaveVoucherDetailsInformation" runat="server" ClientIDMode="Static" CssClass="btn btn-success" Text="Save Voucher Details Information" OnClick="btnSaveVoucherSingleDetailInformation_Click" />
                              
                                 <input class="btn btn-warning" runat="server" onserverclick="btnVoucherSingleDetailInformationClear_Click" type="button" value="Clear Information" />
                            </div>
                        </div>
                       <div class="box">
                           
                            <div class="box-body">
                                <div id="divVoucherDetailsInformationTable" class="dataTables_wrapper form-inline dt-bootstrap">
                                    <div class="row">
                                        <div class="col-sm-6"></div>
                                        <div class="col-sm-6"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table id="voucherDetailsTable" class="table table-bordered table-hover dataTable">
                                                <thead>
                                                    <tr role="row">
                                                        <th>Voucher Descriptiom</th>
                                                        <th>Amount</th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody id="voucherDetailsTableBody" runat="server">
                                                </tbody>
                                               
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <asp:Button ID="btnSaveVoucherDetailsAllInformation" runat="server" ClientIDMode="Static" CssClass="btn btn-success" Text="Save All Voucher  Information" OnClick="btnSaveVoucherDetailsAllInformation_Click" />
                              
                                 <input class="btn btn-warning" runat="server" onserverclick="btnVoucherDetailsAllInformationClear_Click" type="button" value="Clear All Information" />
                            </div>
                        </div>

                </div>
            </div>
        </section>
    </form>
    <script>
        $(document).ready(function() {
            $("#voucherDetailsTable").DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "scrollX": true
            });
        });
    </script>
</asp:Content>
