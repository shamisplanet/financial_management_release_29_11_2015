﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Balancika.Bll;
using Balancika.BLL;
using BALANCIKA.BLL;

namespace Balancika
{
    public partial class VoucherInformation : System.Web.UI.Page
    {
        private bool isNewEntry;
        private Users user;
        private Company _company = new Company();
      
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["customVoucherMessage"] != null)
            {
                string msg = (string) Session["customVoucherMessage"];
                Alert.Show(msg);
                Session["customVoucherMessage"] = null;
            }
            _company = (Company)Session["Company"];
            if (!isValidSession())
            {
                string str = Request.QueryString.ToString();
                if (str == string.Empty)
                {
                    Response.Redirect("LogIn.aspx?refPage=index.aspx");
                }
                else
                {
                    Response.Redirect("LogIn.aspx?refPage=index.aspx?" + str);
                }

            }
            if (!IsPostBack)
            {
                this.LoadFirstTime();
            }


        }

        private void LoadFirstTime()
        {
            this.LoadChatOfAccountDropDownList();
            this.LoadCostCenterDropDownList();
            this.LoadPaymentModeDropDownList();
            Session["VoucherMasterInformation"] = null;
            Session["VoucherDetails"] = null;
            divVoucherMasterInformation.Visible = true;
            divVoucherDetailsInformation.Visible = false;

        }

        private void LoadChatOfAccountDropDownList()
        {
            List<ChartOfAccount> coaList = new ChartOfAccount().GetAllChartOfAccount(_company.CompanyId);
            coaDropDownList.DataSource = coaList;
            coaDropDownList.DataTextField = "CoaTitle";
            coaDropDownList.DataValueField = "CoaId";
            coaDropDownList.DataBind();
        }

        private void LoadCostCenterDropDownList()
        {
            List<CostCenter> costCenterList = new CostCenter().GetAllCostCenter(_company.CompanyId);
            costCenterDropDownlist.DataSource = costCenterList;
            costCenterDropDownlist.DataTextField = "CostCenterName";
            costCenterDropDownlist.DataValueField = "CostCenterId";
            costCenterDropDownlist.DataBind();
        }

        private void LoadPaymentModeDropDownList()
        {
            List<string> paymentMode = new List<string>()
            {
                "Cash",
                "Bank"
            };
            paymentModeDropDownList.DataSource = paymentMode;
            paymentModeDropDownList.DataBind();
        }

        private bool isValidSession()
        {
            if (Session["user"] == null)
            {
                return false;

            }
            user = (Users)Session["user"];
            return user.UserId != 0;

        }

        private void ClearMaster()
        {
            costCenterDropDownlist.SelectedIndex =
                coaDropDownList.SelectedIndex = paymentModeDropDownList.SelectedIndex = -1;
            voucherDatePicker.SelectedDate = null;
            txtVoucherTotal.Value =
                txtVoucherDescription.Value =
                    txtVoucherNo.Value =
                        txtPaymentRef.Value = "";


        }

        protected void btnSaveVoucherMasterInfomation_Click(object sender, EventArgs e)
        {
            try
            {
                VoucherMaster objVoucherMaster = new VoucherMaster();
                objVoucherMaster.VoucherId = new VoucherMaster().GetMaxVoucherMasterId() + 1;
                objVoucherMaster.VoucherDate = voucherDatePicker.SelectedDate.ToString();
                objVoucherMaster.VoucherNo = txtVoucherNo.Value;

                objVoucherMaster.VoucherType = "Voucher";
                objVoucherMaster.CompanyId = _company.CompanyId;

                objVoucherMaster.CostCenterId = int.Parse(costCenterDropDownlist.SelectedItem.Value);
                objVoucherMaster.SourceCOAId = int.Parse(coaDropDownList.SelectedItem.Value);
                objVoucherMaster.PaymentMode = paymentModeDropDownList.SelectedText.ToString();
                objVoucherMaster.VoucherDescription = txtVoucherDescription.Value;

                objVoucherMaster.UpdateBy = user.UserId;
                objVoucherMaster.UpdateDate = DateTime.Now;
                objVoucherMaster.ApprovedBy = user.UserId;
                objVoucherMaster.ApprovedDate = DateTime.Now;
                objVoucherMaster.PostDate = DateTime.Now;
                objVoucherMaster.VoucherTotal = txtVoucherTotal.Value;
                objVoucherMaster.PaymentRef = txtPaymentRef.Value;
                objVoucherMaster.IsPosted = true;
                objVoucherMaster.Status = "Checked";
                Session["VoucherMasterInformation"] = objVoucherMaster;

                divVoucherMasterInformation.Visible = false;
                divVoucherDetailsInformation.Visible = true;


            }
            catch (Exception ex)
            {
                Alert.Show(ex.Message);
            }
        }

        protected void btnVoucherMasterInfomationClear_Click(object sender, EventArgs e)
        {
            this.ClearMaster();
        }

        protected void btnSaveVoucherSingleDetailInformation_Click(object sender, EventArgs e)
        {
            try
            {

                VoucherMaster tmpVoucherMaster = (VoucherMaster) Session["VoucherMasterInformation"];
                List<VoucherDetail> voucherDetailsList = new List<VoucherDetail>();
                VoucherDetail objVoucherDetail = new VoucherDetail();
                if (Session["VoucherDetails"] == null)
                {
                    objVoucherDetail.VoucherDetailId = new VoucherDetail().GetMaxVoucherDetailsId() + 1;

                }
                else
                {
                    voucherDetailsList = (List<VoucherDetail>) Session["VoucherDetails"];
                    VoucherDetail tempVoucher = voucherDetailsList[voucherDetailsList.Count - 1];
                    objVoucherDetail.VoucherDetailId = tempVoucher.VoucherId + 1;
                }
                objVoucherDetail.COAId = tmpVoucherMaster.SourceCOAId;
                objVoucherDetail.VoucherId = tmpVoucherMaster.VoucherId;
                objVoucherDetail.LineDesciption = txtVoucherDetailsDescription.Value;
                objVoucherDetail.Amount = Convert.ToDecimal(txtVoucherAmount.Value);
                voucherDetailsList.Add(objVoucherDetail);
                Session["VoucherDetails"] = voucherDetailsList;
                LoadVoucherTable(voucherDetailsList);
            }
            catch (Exception ex)
            {
                Alert.Show(ex.Message);
            }


        }

        public void LoadVoucherTable(List<VoucherDetail> tmpList)
        {
            voucherDetailsTableBody.InnerHtml = "";
            string tempHtml = "";
            foreach (VoucherDetail voucher in tmpList)
            {
                tempHtml += String.Format(@"<tr><th>{0}</th><th>{1}</th></tr>",voucher.LineDesciption,voucher.Amount);
            }
            voucherDetailsTableBody.InnerHtml += tempHtml;
        }

        protected void btnVoucherSingleDetailInformationClear_Click(object sender, EventArgs e)
        {
            txtVoucherAmount.Value = txtVoucherAmount.Value = "";

        }

        protected void btnSaveVoucherDetailsAllInformation_Click(object sender, EventArgs e)
        {
            try
            {

                VoucherMaster objVoucher = (VoucherMaster) Session["VoucherMasterInformation"];
                List<VoucherDetail> tempoVoucherDetails = (List<VoucherDetail>) Session["VoucherDetails"];
                int chk1 = objVoucher.InsertVoucherMaster();
                foreach (VoucherDetail voucher in tempoVoucherDetails)
                {
                    int chk2 = voucher.InsertVoucherDetail();
                    if (chk2 < 0|| chk1<0)
                    {
                        Alert.Show("Error Occured while inserting voucher details");
                        
                    }
                }
                Session["customVoucherMessage"] = "Saved Voucher Information Successfully";
                Response.Redirect("VoucherInformation.aspx", true);
            }
            catch (Exception ex)
            {
                Alert.Show(ex.Message);
            }


        }

        protected void btnVoucherDetailsAllInformationClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("VoucherInformation.aspx", true);
        }
    }
}